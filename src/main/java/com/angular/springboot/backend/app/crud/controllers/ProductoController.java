package com.angular.springboot.backend.app.crud.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.angular.springboot.backend.app.crud.entities.Producto;
import com.angular.springboot.backend.app.crud.services.IProductoService;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = {"*"})
public class ProductoController {
	
	@Autowired
	private IProductoService productService;
	
	//ruta para traer todos los registros de la base de datos
	@GetMapping("/listar")
	public List<Producto> index(){
		return productService.getProducts();
	}
	
	@GetMapping("/mostrar/{id}")
	public  ResponseEntity<?> showProduct(@PathVariable Long id) {
		
		Producto producto = null;
		Map<String,Object> response = new HashMap<>();
		
		//capturar error que se generaria por problemas con la conexion
		try {
			
			producto = productService.findById(id);
			
		} catch (DataAccessException e) {
			
			response.put("mensaje","Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		//capturando error especifico en caso de que el id no exista
		if(producto == null) {
			response.put("mensaje", "El producto con Id: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}
	
	//ruta para guardar nuevo registro
	@PostMapping("/mostrar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@RequestBody Producto product) {
		
		Producto producto = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			
			producto = productService.saveProduct(product);
			
		} catch (DataAccessException e) {
			
			response.put("mensaje","Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}			
			response.put("mensaje", "El producto ha sido creado con exito");
			response.put("Producto", producto);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@ResponseStatus(HttpStatus.CREATED)//ruta para editar un registro de la base de datos
	@PutMapping("/editar/{id}")
	//ruta que traslada un registro de la base de datos a la vista para modificarlo
	public ResponseEntity<?> update(@RequestBody Producto product, @PathVariable Long id) {
		
		Producto productoUpdated = null;
		Producto productoBaseDatos = productService.findById(id);
		
		Map<String,Object> response = new HashMap<>();
		
		if(productoBaseDatos == null) {
			response.put("error", "Error, no se pudo editar el producto con id ".concat(id.toString().concat(". No existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			
			productoBaseDatos.setNombre(product.getNombre());
			productoBaseDatos.setPrecio(product.getPrecio());
			productoBaseDatos.setTipoProducto(product.getTipoProducto());
			
			productoUpdated = productService.saveProduct(productoBaseDatos);
			
		} catch (DataAccessException e) {
				
			response.put("mensaje","Error al actualizar el producto en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}	
		
		response.put("mensaje", "El producto ha sido actualizado con exito");
		response.put("Producto", productoUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable Long id) {
		
		Map<String,Object> response = new HashMap<>();
		
		try {
			
			productService.deleteById(id);
			
		} catch (DataAccessException e) {
				
			response.put("mensaje","Error al eliminar el producto en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		response.put("mensaje", "Error al eliminar el producto de la base de datos");
		return new ResponseEntity<Map<String,Object>>(response, HttpStatus.OK);
	}

}
